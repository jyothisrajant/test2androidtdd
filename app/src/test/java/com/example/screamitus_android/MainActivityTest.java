package com.example.screamitus_android;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.junit.Test;

import static junit.framework.Assert.assertNotNull;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class MainActivityTest {
    private MainActivity activity;
    @Test
    public void tc1ApploadCase() throws Exception {


    }
    @Test
    public void tc2ResultCorrect() throws Exception {
        EditText daysTxt = (EditText) activity.findViewById(R.id.daysTextBox);
        TextView resultTxt = (EditText) activity.findViewById(R.id.resultsLabel);
        daysTxt.setText("5");
        //Initialize calculatebutton
        Button calculateButton = (Button) activity.findViewById(R.id.btn);

        //Click on save button
        calculateButton.performClick();
        assertThat(resultTxt.getText().toString(), equalTo("25 instructors infected") );

    }

}
