package com.example.screamitus_android;

public class Infection {
    //variable for no. of instructors
    int noOfInstructors;

//function for calculating total number of infected instructors
    int calculateTotalInfected(int day){

        //R1 Condition
        if(day>0){
            //R4 Condition
            if(day/2==0) {

                noOfInstructors=0;
            }
            else{
                //R2 Condition

            if(day<=7){

                noOfInstructors=5*day;
            }
            //R3 Condition
            else{

                noOfInstructors=(35)+((day-7)*8);
            }
            }
        }
        else{

            noOfInstructors=-1;
        }
        return noOfInstructors;
    }
}
